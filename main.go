package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB
var err error

const DNS = "root:@tcp(127.0.0.1:3306)/gorm_mysql3?charset=utf8mb4&parseTime=True&loc=Local"

type User struct {
	gorm.Model
	Email    string `json:"email"`
	Password string `json:"password"`
}

func initialMigration() {
	DB, err = gorm.Open(mysql.Open(DNS), &gorm.Config{})
	if err != nil {
		fmt.Println(err.Error())
		panic("Cannot connect to DB")
	}
	DB.AutoMigrate(&User{})
}

func initialRouter() {
	r := mux.NewRouter()
	r.HandleFunc("/register", register).Methods("POST")
	log.Fatal(http.ListenAndServe(":3000", r))
}

func register(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var user User
	json.NewDecoder(r.Body).Decode(&user)
	DB.Create(&user)
	json.NewEncoder(w).Encode(user)
}

func main() {
	initialMigration()
	initialRouter()
}
